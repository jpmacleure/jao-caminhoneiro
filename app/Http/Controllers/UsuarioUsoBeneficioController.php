<?php

namespace App\Http\Controllers;

use App\UsuarioUsoBeneficio;
use Illuminate\Http\Request;

class UsuarioUsoBeneficioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioUsoBeneficio  $usuarioUsoBeneficio
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioUsoBeneficio $usuarioUsoBeneficio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioUsoBeneficio  $usuarioUsoBeneficio
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioUsoBeneficio $usuarioUsoBeneficio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioUsoBeneficio  $usuarioUsoBeneficio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioUsoBeneficio $usuarioUsoBeneficio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioUsoBeneficio  $usuarioUsoBeneficio
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioUsoBeneficio $usuarioUsoBeneficio)
    {
        //
    }
}
