<?php

namespace App\Http\Controllers;

use App\UsuarioEmblema;
use Illuminate\Http\Request;

class UsuarioEmblemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioEmblema  $usuarioEmblema
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioEmblema $usuarioEmblema)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioEmblema  $usuarioEmblema
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioEmblema $usuarioEmblema)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioEmblema  $usuarioEmblema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioEmblema $usuarioEmblema)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioEmblema  $usuarioEmblema
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioEmblema $usuarioEmblema)
    {
        //
    }
}
