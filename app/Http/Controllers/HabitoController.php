<?php

namespace App\Http\Controllers;

use App\Habito;
use Illuminate\Http\Request;

class HabitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Habito  $habito
     * @return \Illuminate\Http\Response
     */
    public function show(Habito $habito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Habito  $habito
     * @return \Illuminate\Http\Response
     */
    public function edit(Habito $habito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Habito  $habito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Habito $habito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Habito  $habito
     * @return \Illuminate\Http\Response
     */
    public function destroy(Habito $habito)
    {
        //
    }
}
