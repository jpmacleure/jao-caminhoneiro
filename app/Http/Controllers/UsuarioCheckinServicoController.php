<?php

namespace App\Http\Controllers;

use App\UsuarioCheckinServico;
use Illuminate\Http\Request;

class UsuarioCheckinServicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioCheckinServico  $usuarioCheckinServico
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioCheckinServico $usuarioCheckinServico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioCheckinServico  $usuarioCheckinServico
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioCheckinServico $usuarioCheckinServico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioCheckinServico  $usuarioCheckinServico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioCheckinServico $usuarioCheckinServico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioCheckinServico  $usuarioCheckinServico
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioCheckinServico $usuarioCheckinServico)
    {
        //
    }
}
