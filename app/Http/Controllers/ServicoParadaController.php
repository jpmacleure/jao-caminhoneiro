<?php

namespace App\Http\Controllers;

use App\ServicoParada;
use Illuminate\Http\Request;

class ServicoParadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicoParada  $servicoParada
     * @return \Illuminate\Http\Response
     */
    public function show(ServicoParada $servicoParada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicoParada  $servicoParada
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicoParada $servicoParada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicoParada  $servicoParada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicoParada $servicoParada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicoParada  $servicoParada
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicoParada $servicoParada)
    {
        //
    }
}
