<?php

namespace App\Http\Controllers;

use App\UsuarioCheckHabito;
use Illuminate\Http\Request;

class UsuarioCheckHabitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioCheckHabito  $usuarioCheckHabito
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioCheckHabito $usuarioCheckHabito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioCheckHabito  $usuarioCheckHabito
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioCheckHabito $usuarioCheckHabito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioCheckHabito  $usuarioCheckHabito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioCheckHabito $usuarioCheckHabito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioCheckHabito  $usuarioCheckHabito
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioCheckHabito $usuarioCheckHabito)
    {
        //
    }
}
