<?php

namespace App\Http\Controllers;

use App\Emblema;
use Illuminate\Http\Request;

class EmblemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emblema  $emblema
     * @return \Illuminate\Http\Response
     */
    public function show(Emblema $emblema)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emblema  $emblema
     * @return \Illuminate\Http\Response
     */
    public function edit(Emblema $emblema)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emblema  $emblema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emblema $emblema)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emblema  $emblema
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emblema $emblema)
    {
        //
    }
}
