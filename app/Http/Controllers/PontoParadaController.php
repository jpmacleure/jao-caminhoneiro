<?php

namespace App\Http\Controllers;

use App\PontoParada;
use Illuminate\Http\Request;

class PontoParadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PontoParada  $pontoParada
     * @return \Illuminate\Http\Response
     */
    public function show(PontoParada $pontoParada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PontoParada  $pontoParada
     * @return \Illuminate\Http\Response
     */
    public function edit(PontoParada $pontoParada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PontoParada  $pontoParada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PontoParada $pontoParada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PontoParada  $pontoParada
     * @return \Illuminate\Http\Response
     */
    public function destroy(PontoParada $pontoParada)
    {
        //
    }
}
