<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="css/style.css">
 
    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
 
</head>

<body>

   <div id="journey-screen" class="container" style="overflow:hidden">
      <div class="row mt-4">
         <div class="col-12">
            <h3 class="font-title">Nova Jornada?</h3>
            <div class="d-flex justify-content-center">
               <img class="mt-3" src="/assets/Dashboard_-_Illustration.svg">
            </div>
            <a class="btn btn-brand btn-lg btn-block mt-3" href="/jornada">Vamos lá!</a>
         </div>
      </div>

      <div class="row mt-5">
         <div class="col-12">
            <h3 class="font-title">Viagens</h3>
            <div class="card border-gray mb-3">
               <div class="row no-gutters">
                  <div class="col-6">
                     <div class="card-body">
                     <h5 class="font-card-title">12 jun - 17 jun</h5>
                     <p class="card-text">This is a wider card who had...</p>
                     </div>
                  </div>
                  <div class="col-6">
                  <img src="assets/2820453.jpg" class="card-img" style="height: 100%!important;" alt="...">
                  </div>
               </div>
            </div>

            <a class="btn btn-md btn-ghost btn-lg btn-block ml-auto" href="/historico">Ver mais...</a>
         </div>
      </div>

   </div>

   <div id="habit-screen" class="container d-none">
      <div class="row mt-2">
         <div class="col-12">
            
            <!-- JÃO PROFILE -->
            <div class="jao-caminhoneiro d-flex">
               <img class="trucao" src="assets/jao.png">
               <div class="jao-current ml-2">
                  <p class="title">Trucão Caminhoneiro</p>
                  <div class="progress">
                     <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25/100 XP</div>
                  </div>
                  <p class="font-help">Level 1</p>
               </div>
            </div>

         </div>
      </div>

      <div class="row">
         <div class="card-habit bg-light mb-3 d-flex">
            <div class="card-body">
              <h5 class="card-habit-title">Exercícios</h5>
              <p class="card-habit-text">Semana 1 de 4</p>
              <div class="d-flex">
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Dom</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Seg</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Ter</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Qua</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Qui</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Sex</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Sáb</span>
                  </div>
              </div>
            </div>
            <div class="arrow-habits d-flex justify-content-center align-items-center">
               <div class="arrow-container">
                  <a href="/detalhe-habito"><i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
          </div>
          <div class="card-habit bg-light mb-3 d-flex">
            <div class="card-body">
              <h5 class="card-habit-title">Comer uma maçã</h5>
              <p class="card-habit-text">Semana 1 de 4</p>
              <div class="d-flex">
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Dom</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Seg</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Ter</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Qua</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Qui</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Sex</span>
                  </div>
                  <div class="day-of-week">
                     <span class="dot"></span>
                     <span class="day">Sáb</span>
                  </div>
              </div>
            </div>
            <div class="arrow-habits d-flex justify-content-center align-items-center">
               <div class="arrow-container">
                  <a href="/detalhe-habito"><i class="fas fa-arrow-right"></i></a>
               </div>
            </div>
          </div>
      </div>
   </div>

   <div id="toggle-screen" class="container d-none">
      <div class="row mt-2">
         <div class="col-12">
            
            <!-- JÃO PROFILE -->
            <div class="jao-caminhoneiro d-flex">
               <img class="trucao" src="assets/jao.png">
               <div class="jao-current ml-2">
                  <p class="title">Trucão Caminhoneiro</p>
                  <div class="progress">
                     <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25/100 XP</div>
                  </div>
                  <p class="font-help" style="margin-bottom: 2px;">Level 1</p>
                  <div class="d-flex">
                     <img src="/assets/Gold Coins - 800x800.png" style="width: 20px; height: 20px;">
                     <p style="font-family: CircularBold; margin-left: 5px;">20</p>
                  </div>
               </div>
            </div>

            <div class="list-group">
               <a href="/emblema" class="list-group-item list-group-item-action">Emblemas</a>
               <a href="/beneficio" class="list-group-item list-group-item-action">Benefícios</a>
               <!-- <a href="/comunidade" class="list-group-item list-group-item-action">Comunidade</a> -->
               <a href="/contato" class="list-group-item list-group-item-action">Contato</a>
             </div>

         </div>
      </div>

      <div class="row">

      </div>
   </div>

   <div class="container-fluid bg-orange-light position-fixed mt-3">
      <div class="row height-max d-flex align-items-center justify-content-between">

         <!-- BUTTON HABITS -->
         <div id="habit" class="button col-4 d-flex justify-content-center align-items-center" onclick='selectButton(this)'>
            <i class="icon-color fas fa-user-clock"></i>
         </div>

         <!-- BUTTON JOURNEY -->
         <div id="journey" class="button col-4 selected d-flex justify-content-center align-items-center" onclick='selectButton(this)'>
            <img class="icon" src="assets/Path_54.svg" class="card-img" style="height: 100%!important;" alt="...">
         </div>

         <!-- BUTTON TOGGLER -->
         <div id="toggler" class="button col-4 d-flex justify-content-center align-items-center" onclick='selectButton(this)'>
            <i class="icon-color fas fa-bars"></i>
         </div>
      </div>
   </div>
          
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

   <script>
      let bottom_button = document.querySelectorAll('.button');

      let s_journey = document.getElementById('journey-screen');
      let s_habit = document.getElementById('habit-screen');
      let s_toggle = document.getElementById('toggle-screen');

      console.log(s_journey);
      console.log(s_habit);
      console.log(s_toggle);

      function selectButton(i){

         bottom_button.forEach(item => {
            item.classList.toggle('selected', item === i);
            let idButton = i.id;

            if(idButton == "journey"){
               s_journey.classList.remove('d-none');
               s_toggle.classList.add('d-none');
               s_habit.classList.add('d-none');
            }

            else if(idButton == "habit"){
               s_habit.classList.remove('d-none');
               s_journey.classList.add('d-none');
               s_toggle.classList.add('d-none');
            }

            else{
               s_toggle.classList.remove('d-none');
               s_journey.classList.add('d-none');
               s_habit.classList.add('d-none');
            }

         });
      }

   </script>

</body>

</html>