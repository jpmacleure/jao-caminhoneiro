<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="css/style.css">

    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
</head>
<body class="max-heigth">
    <div class="container">
         <!--Titulo-->
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="font-title pt-5 pb-4">Coloque seu numero e divirta-se!</h3>
                <label class="field-form mb-4" for="name">
                  <input class="field " name="name" type="text" required autocomplete="off">
                  <span class="label">Telefone</span>
                </label>
                <a class="btn btn-brand btn-lg btn-block" href="/home">Entrar</a>
                <p class="font-help text-center mt-1">Ainda não tem uma conta? <a class="" href="/cadastro">Cadastre-se</a></p>
                <img src="/src/assets/Login-illustration.svg" class="img-login">
            </div>
          </div>
      </div>
    
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>   
</body>
</html>
