<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="../css/style.css">
 
    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="../css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
 
</head>

<body>

    <div class="container mt-2 d-flex justify-content-between">
        <a href="/home" class="arrow-container">
            <i class="fas fa-arrow-left"></i>
         </a>

        <div class="trophy-quanty mt-1">
            <i class="fas fa-trophy"></i>
            <p class="numbers">1/15</p>
        </div>
    </div>

    <div class="container mt-4">
        <p class="emblema-title">Liberados</p>
        <div>
            <div class="card-emblema mb-3">
                <div class="row no-gutters">
                    <div class="col-2">
                        <img src="/assets/policia-emblema.png" class="img-emblema"alt="...">
                    </div>
                   <div class="col-10">
                      <div class="card-body p-2 pl-3">
                      <h5 class="font-card-title mb-0">Paisana</h5>
                      <p class="card-emblema-text mb-0">Denúncia de estabelecimento validada</p>
                      <p class="card-emblema-text mb-0">• 50 PX</p>
                      </div>
                   </div>
                </div>
             </div>
        </div>
    </div>

    <div class="container mt-4">
        <p class="emblema-title">Bloqueados</p>
        <div>
            <div class="card-emblema mb-3">
                <div class="row no-gutters">
                    <div class="col-2">
                        <img src="/assets/lock-emblema.png" class="img-emblema"alt="...">
                    </div>
                   <div class="col-10">
                      <div class="card-body p-2 pl-3">
                      <h5 class="font-card-title mb-0">Paisana</h5>
                      <p class="card-emblema-text mb-0">Denúncia de estabelecimento validada</p>
                      <p class="card-emblema-text mb-0">• 50 PX</p>
                      </div>
                   </div>
                </div>
             </div>
             <div class="card-emblema mb-3">
                <div class="row no-gutters">
                    <div class="col-2">
                        <img src="/assets/lock-emblema.png" class="img-emblema"alt="...">
                    </div>
                   <div class="col-10">
                      <div class="card-body p-2 pl-3">
                      <h5 class="font-card-title mb-0">Paisana</h5>
                      <p class="card-emblema-text mb-0">Denúncia de estabelecimento validada</p>
                      <p class="card-emblema-text mb-0">• 50 PX</p>
                      </div>
                   </div>
                </div>
             </div>
             <div class="card-emblema mb-3">
                <div class="row no-gutters">
                    <div class="col-2">
                        <img src="/assets/lock-emblema.png" class="img-emblema"alt="...">
                    </div>
                   <div class="col-10">
                      <div class="card-body p-2 pl-3">
                      <h5 class="font-card-title mb-0">Paisana</h5>
                      <p class="card-emblema-text mb-0">Denúncia de estabelecimento validada</p>
                      <p class="card-emblema-text mb-0">• 50 PX</p>
                      </div>
                   </div>
                </div>
             </div>
             <div class="card-emblema mb-3">
                <div class="row no-gutters">
                    <div class="col-2">
                        <img src="/assets/lock-emblema.png" class="img-emblema"alt="...">
                    </div>
                   <div class="col-10">
                      <div class="card-body p-2 pl-3">
                      <h5 class="font-card-title mb-0">Paisana</h5>
                      <p class="card-emblema-text mb-0">Denúncia de estabelecimento validada</p>
                      <p class="card-emblema-text mb-0">• 50 PX</p>
                      </div>
                   </div>
                </div>
             </div>
        </div>
    </div>

          
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>