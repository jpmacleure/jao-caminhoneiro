<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="css/style.css">
 
    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
 
</head>
<body class="max-heigth">
    <div class="container">
        <div class="row">
            <div class="col-12">

               <h3 class="text-center font-title pt-5 pb-4">Cadastre-se e descubra um mundo de diversão nas estradas!</h3>

               <div class="mb-4">
                  <label class="field-form m-auto" for="name">
                    <input class="field " name="name" type="text" required autocomplete="off">
                    <span class="label">Eu me chamo...</span>
                  </label>
                  <p class="font-help ml-1">Seu nome, sobrenome ou apelido</p>
                </div>

                <div class="mb-4">
                  <label class="field-form m-auto" for="name">
                    <input class="field " name="name" type="text" required autocomplete="on">
                    <span class="label">Meu telefone é...</span>
                  </label>
                  <p class="font-help ml-1">DDD + Número</p>
                </div>

                <a class="btn btn-brand btn-lg btn-block" href="/home">Enviar</a>
                <p class="font-help text-center mt-1">Já é cadastrado? Faça <a class="" href="/login">Login</a></p>

                <img src="/src/assets/Register_-_Illustration.svg" class="img-register">
            </div>
         </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>   
</body>

</html>
