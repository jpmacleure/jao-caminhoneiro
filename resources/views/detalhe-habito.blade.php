<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="/css/style.css">
 
    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
 
</head>

<body>

 
    <div class="container">
        <div class="d-flex">
            <img class="trucao" src="/assets/jao.png" style="margin-left: 160px; margin-top:  50px;">
        
        
        </div>
    
        <div>
            <h4 class="font-title text-center mt-3">Exercicios</h4>
            <p class="font-help text-center">1 vez por dia, 7 dias por semana em</p>
            <p class="font-help text-center">4 dias por semana</p>
        </div>



        <div>
            <button class="btn btn-habito btn-lg btn-block">Você ainda não cumpriu seu hábito</button>
        </div>
    
        <div class="mt-5 ml-5">
            <div class="row">
                <i class="fas fa-bell fa-2x col-2 align-content-center" style="color: #fc9700;"></i>
                <p class="font-common align-content-center">Ativar lembrete</p>


            </div>

            <div class="row mt-3">
                <i class="fas fa-calendar fa-2x col-2 align-content-center" style="color: #fc9700;"></i>
                <p class="font-common align-content-center">Caléndario e estatisticas</p>


            </div>
        </div>
    
        <div class="mt-5 ml-2">
            <h3 class="font-common justify-text">Detalhes</h3>
            <p class="font-help justify-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sem elit, rhoncus nec facilisis eu, vulputate ut massa. Nullam quis neque suscipit, tristique nisl sit amet, feugiat est. Donec posuere efficitur lorem nec dignissim. Praesent a risus velit. Curabitur auctor lacus luctus, posuere lacus et, congue metus.</p>
        </div>
    </div>

          
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>