<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="/css/style.css">
 
    
    <!-- Manifest-->

    <link rel="manifest" href="manifest.json">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">  
    <link rel="stylesheet" type="text/css" href="css/fonts.css">


     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 
 
</head>

    <div class="container-fluid">
        <div class="row justify-content-center" style=" margin-top: 50px; margin-left: 30px;">
            <div class="col-12">
               <h3 class="text-left font-title" >Essas são suas</h3>
               <h3 class="text-left font-title" style="font-family: cursive;">ultimas viajens...</h3>
 
            </div>
         </div>

        <div class="row align-content-center border-top border-bottom my-4">
            <div class="col-6 align-self-center">
               <h4 class="text-center font-title">12 jun - 17 jun</h4>
               <p class="justify-text ml-4 font-common"><a href="/historico" style="color: black;">a or   em ipsum dolor sit amet, consectetur adipiscing elit</a>.</p>
            </div>

            <div class="col-6">
                <img src="/assets/2820453.jpg" class="img-fluid" width="98%">

            </div>


        </div>

        
        <div class="row align-content-center border-top border-bottom my-4">
            <div class="col-6 align-self-center">
               <h4 class="text-center font-title">12 jun - 17 jun</h4>
               <p class="justify-text ml-4 font-common"><a href="/historico" style="color: black;">a or   em ipsum dolor sit amet, consectetur adipiscing elit</a>.</p>
            </div>

            <div class="col-6">
                <img src="/assets/2820453.jpg" class="img-fluid" width="98%">

            </div>


        </div>

        
        <div class="row align-content-center border-top border-bottom my-4">
            <div class="col-6 align-self-center">
               <h4 class="text-center font-title">12 jun - 17 jun</h4>
               <p class="justify-text ml-4 font-common"><a href="/historico" style="color: black;">a or   em ipsum dolor sit amet, consectetur adipiscing elit</a>.</p>
            </div>

            <div class="col-6">
                <img src="/assets/2820453.jpg" class="img-fluid" width="98%">

            </div>


        </div>


        
        <div class="row align-content-center border-top border-bottom my-4">
            <div class="col-6 align-self-center">
               <h4 class="text-center font-title">12 jun - 17 jun</h4>
               <p class="justify-text ml-4 font-common"><a href="/historico" style="color: black;">a or   em ipsum dolor sit amet, consectetur adipiscing elit</a>.</p>
            </div>

            <div class="col-6">
                <img src="/assets/2820453.jpg" class="img-fluid" width="98%">

            </div>


        </div>

        
              <!-- Modal 1  -->
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      
                      <img src="/assets/2820453.jpg" class="img-fluid">
                    </div>
                    <div class="modal-body">
                        <h5 class="modal-title text-center" id="exampleModalLabel"> 12 a 14 jun</h5>
                    </div>
                    <div class="modal-footer">
                 
                    </div>
                  </div>
                </div>

        
         
    </div>
    
    

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>   
</body>


  

</html>
