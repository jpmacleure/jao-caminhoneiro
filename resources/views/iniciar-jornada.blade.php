<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link async rel="stylesheet" href="/css/style.css">
 
    <!-- Manifest-->
    <link rel="manifest" href="manifest.json">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/css/bootstrap.css">

     <!-- Font Awesome -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
     

    <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.css' rel='stylesheet'/>
 
</head>

<body>

    <div id='map' style='width: 400px; height: 300px;'></div>

    <div class="container mt-4 ml-3">
        <div class="row mb-4">
            <div class="circle-points">
                <p class="number">P1<br><span class="hours">2 hs</span></p>
            </div>

            <div class="stop-points ml-3">
                <p class="title">Ponto de parada 1</p>
                <p class="act">
                    <span class="gren-point">•</span> 5 atividades - <span class="bold-text">1500 XP</span>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="circle-points">
                <p class="number">P2<br><span class="hours">4 hs</span></p>
            </div>
            
            <div class="stop-points ml-3">
                <p class="title">Ponto de parada, chegada</p>
                <p class="act">
                    <span class="gren-point">•</span> <a ></a> <span class="bold-text">1500 XP</span>
                </p>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="d-flex justify-content-around">
            <a class="btn btn-journey cancel btn-lg" href="/jornada">Cancelar</a>
            <a class="btn btn-journey init btn-lg" href="/home">Iniciar</a>
        </div>
    </div>


    
    <!--Pop UP sobre ponto de parada-->
    
    
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              
              <img src="/assets/2820453.jpg" class="img-fluid">
            </div>
            <div class="modal-body">
                <h5 class="modal-title text-center" id="exampleModalLabel"> 12 a 14 jun</h5>
            </div>
            <div class="modal-footer">
         
            </div>
          </div>
        </div>


 
    </div>      
          
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

   <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoianBtYWNsZXVyZSIsImEiOiJjamw4enNlZjUzb2JzM3FwNDdhemN4dTQwIn0.dAaw0aLPA9eg5NSqPqyHsA';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11'
        });
    </script>
</body>
</html>