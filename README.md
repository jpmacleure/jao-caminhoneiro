<!-- 
<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>
-->

# jao-caminhoneiro

É um projeto de software entregue durante o:

+ [Hackathon CCR](http://www.grupoccr.com.br/hackathonccr/)

### Instruções de uso

Para executar o projeto, é preciso ter um ambiente configurado com
- [Docker](https://docs.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

#### Comandos iniciais

Após clonar o repositório do projeto, navegue até a pasta do download e copie o arquivo '.env.example' para '.env'

```
$ cd jao-caminhoneiro
$ cp .env.example .env
```

Instale as dependências do projeto utilizando a imagem docker do php compose

```
$ docker run --rm -v $(pwd):/app composer install
```

Em seguida, execute o seguinte comando para iniciar a aplicação

```
$ docker-compose up -d
```

Após iniciada, é preciso gerar uma chave ssh, que será armazenada no arquivi '.env'. Para gerar a chave ssh do app, use o seguinte comando

```
$ docker-compose exec jao-caminhoneiro-app php artisan key:generate
```

Feito isso, basta executar as migrations/seeds para inicializar o BD com dados de teste

```
$ docker-compose exec jao-caminhoneiro-app php artisan migrate:fresh --seed
```

Para verificar se a aplicação está respondendo, acesse
[http://localhost:1003/]

Para verificar as rotas disponíveis, utilize o o comando

```
$ docker-compose exec jao-caminhoneiro-app php artisan route:list
```

Para executar os testes unitários e de integração, execute:

```
$ docker-compose exec jao-caminhoneiro-app php ./vendor/bin/phpunit
```