<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePontoParadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ponto_paradas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_trajeto');
            $table->text('tx_nome_parada');
            $table->double('nr_lat_parada', 20, 10);
            $table->double('nr_lon_parada', 20, 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ponto_paradas');
    }
}
