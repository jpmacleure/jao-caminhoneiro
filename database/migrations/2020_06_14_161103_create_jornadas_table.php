<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJornadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('jornadas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_trajeto');
            $table->integer('dt_inicio');
            $table->integer('dt_fim');
            $table->text('tx_descricao');
            $table->text('tx_link_foto_jornada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jornadas');
    }
}
