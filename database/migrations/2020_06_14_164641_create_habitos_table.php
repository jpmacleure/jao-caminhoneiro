<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('habitos', function (Blueprint $table) {
            $table->id();
            $table->string('tx_nome_habito');
            $table->text('tx_descricao_habito');
            $table->integer('nr_qtd_vezes_dia');
            $table->integer('nr_qtd_dias');
            $table->integer('nr_frequencia');
            $table->text('tx_link_foto_habito');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitos');
    }
}
