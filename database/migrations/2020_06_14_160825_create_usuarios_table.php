<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->string('tx_apelido');
            $table->string('nr_telefone');
            $table->integer('dt_nascimento');
            $table->double('nr_peso_kg');
            $table->double('nr_altura_mt');
            $table->integer('nr_freq_exercicios');
            $table->integer('nr_qtd_truck_coins');
            $table->text('tx_link_foto_usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
