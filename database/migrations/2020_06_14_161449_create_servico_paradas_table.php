<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicoParadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('servico_paradas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_ponto_parada');
            $table->string('tx_nome_servico');
            $table->string('tx_segmento_servico');
            $table->text('tx_descricao_servico');
            $table->integer('nr_exp_checkin_servico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servico_paradas');
    }
}
