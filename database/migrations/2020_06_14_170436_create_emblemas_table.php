<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmblemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('emblemas', function (Blueprint $table) {
            $table->id();
            $table->string('tx_nome_emblema');
            $table->text('tx_descricao_emblema');
            $table->integer('nr_exp_debloqueio_emblema');
            $table->text('tx_link_foto_emblema');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emblemas');
    }
}
