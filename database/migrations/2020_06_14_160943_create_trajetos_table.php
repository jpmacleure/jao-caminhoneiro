<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrajetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::create('trajetos', function (Blueprint $table) {
            $table->id();
            $table->string('tx_cidade_partida');
            $table->double('nr_lat_cidade_partida', 20, 10);
            $table->double('nr_lon_cidade_partida', 20, 10);
            $table->string('tx_cidade_destino');
            $table->double('nr_lat_cidade_destino', 20, 10);
            $table->double('nr_lon_cidade_destino', 20, 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trajetos');
    }
}
