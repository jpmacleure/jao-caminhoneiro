<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('beneficios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_servico_parada');
            $table->string('tx_nome_beneficio');
            $table->text('tx_descricao_beneficio');
            $table->integer('nr_valor_truck_coins');
            $table->integer('nr_exp_uso_beneficio');
            $table->text('tx_link_foto_beneficio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficios');
    }
}
