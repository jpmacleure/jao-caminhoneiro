<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsuarioCheckHabito;
use Faker\Generator as Faker;

$factory->define(UsuarioCheckHabito::class, function (Faker $faker) {
    return [
        'id_usuario_habito' => 0
    ];
});
