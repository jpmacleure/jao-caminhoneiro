<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Emblema;
use Faker\Generator as Faker;

$factory->define(Emblema::class, function (Faker $faker) {
    return [
        'tx_nome_emblema' => $faker->word,
        'tx_descricao_emblema' => $faker->sentence,
        'nr_exp_debloqueio_emblema' => rand(10, 100),
        'tx_link_foto_emblema' => ''
    ];
});