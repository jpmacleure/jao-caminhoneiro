<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServicoParada;
use Faker\Generator as Faker;

$factory->define(ServicoParada::class, function (Faker $faker) {
    return [
        'id_ponto_parada' => 0,
        'tx_nome_servico' => $faker->word,
        'tx_segmento_servico' => $faker->word,
        'tx_descricao_servico' => $faker->sentence,
        'nr_exp_checkin_servico' => rand(10, 100)
    ];
});
