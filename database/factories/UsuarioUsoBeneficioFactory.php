<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsuarioUsoBeneficio;
use Faker\Generator as Faker;

$factory->define(UsuarioUsoBeneficio::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'id_beneficio' => 0,
        'nr_valor_truck_coins' => rand(10, 100),
        'nr_exp_uso_beneficio' => rand(10, 100)
    ];
});
