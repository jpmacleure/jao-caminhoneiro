<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Usuario;
use Faker\Generator as Faker;

$factory->define(Usuario::class, function (Faker $faker) {
    $cd_setor = rand(1, 3);
    $tx_setor = ['INDÚSTRIAS', 'ALIMENTOS', 'FINANCEIRO'];

    return [
        'tx_apelido' => $faker->firstName,
        'nr_telefone' => $faker->phoneNumber,
        'dt_nascimento' => rand(1960, 1999)*10000 + rand(1, 12)*100 + rand(1, 28) , //AAAAMMDD
        'nr_peso_kg' => rand(60.0, 130.0),
        'nr_altura_mt' => rand(1.5, 2.0),
        'nr_freq_exercicios' => rand(0, 2),
        'nr_qtd_truck_coins' => rand(0, 500),
        'tx_link_foto_usuario' => $faker->imageUrl($width = 100, $height = 100, 'cats') 
    ];
});
	