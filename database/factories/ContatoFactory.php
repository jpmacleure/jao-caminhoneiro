<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contato;
use Faker\Generator as Faker;

$factory->define(Contato::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'tx_mensagem_contato' => $faker->sentence,
        'in_leitura' => rand(0, 1)
    ];
});
