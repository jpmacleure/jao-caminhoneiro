<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsuarioCheckinServico;
use Faker\Generator as Faker;

$factory->define(UsuarioCheckinServico::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'id_servico_parada' => 0
    ];
});