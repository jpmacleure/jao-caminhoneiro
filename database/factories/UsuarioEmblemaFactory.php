<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsuarioEmblema;
use Faker\Generator as Faker;

$factory->define(UsuarioEmblema::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'id_emblema' => 0
    ];
});