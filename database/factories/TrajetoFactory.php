<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Trajeto;
use Faker\Generator as Faker;

$factory->define(Trajeto::class, function (Faker $faker) {
    return [
        'tx_cidade_partida' => $faker->city,
        'nr_lat_cidade_partida' => $faker->latitude,
        'nr_lon_cidade_partida' => $faker->longitude,
        'tx_cidade_destino' => $faker->city,
        'nr_lat_cidade_destino' => $faker->latitude,
        'nr_lon_cidade_destino' => $faker->longitude
    ];
});