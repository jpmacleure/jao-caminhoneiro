<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Beneficio;
use Faker\Generator as Faker;

$factory->define(Beneficio::class, function (Faker $faker) {
    return [
        'id_servico_parada' => 0,
        'tx_nome_beneficio' => $faker->word,
        'tx_descricao_beneficio' => $faker->sentence,
        'nr_valor_truck_coins' => rand(1, 500),
        'nr_exp_uso_beneficio' => rand(10, 100),
        'tx_link_foto_beneficio' => ''
    ];
});