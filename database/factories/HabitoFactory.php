<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Habito;
use Faker\Generator as Faker;

$factory->define(Habito::class, function (Faker $faker) {
    return [
        'tx_nome_habito' => $faker->word,
        'tx_descricao_habito' => $faker->sentence,
        'nr_qtd_vezes_dia' => rand(1, 10),
        'nr_qtd_dias' => rand(1, 7),
        'nr_frequencia' => rand(1, 3),
        'tx_link_foto_habito' => ''
    ];  
});