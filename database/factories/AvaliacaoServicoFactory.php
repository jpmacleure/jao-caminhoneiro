<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AvaliacaoServico;
use Faker\Generator as Faker;

$factory->define(AvaliacaoServico::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'id_servico_parada' => 0,
        'nr_valor_avaliacao' => rand(1, 5),
        'tx_comentario_avaliacao' => $faker->sentence
    ];
});