<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsuarioHabito;
use Faker\Generator as Faker;

$factory->define(UsuarioHabito::class, function (Faker $faker) {
    return [
        'id_usuario' => 0,
        'id_habito' => 0,
        'in_lembrete' => rand(0, 1)
    ];
});