<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Jornada;
use Faker\Generator as Faker;

$factory->define(Jornada::class, function (Faker $faker) {
    return [
        'id_trajeto' => 0,
        'dt_inicio' => rand(1960, 1999)*10000 + rand(1, 12)*100 + rand(1, 28) , //AAAAMMDD
        'dt_fim' => rand(1960, 1999)*10000 + rand(1, 12)*100 + rand(1, 28) , //AAAAMMDD
        'tx_descricao' => $faker->sentence,
        'tx_link_foto_jornada' => ''
    ];
});