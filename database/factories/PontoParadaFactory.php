<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PontoParada;
use Faker\Generator as Faker;

$factory->define(PontoParada::class, function (Faker $faker) {
    return [
        'id_trajeto' => 0,
        'tx_nome_parada' => $faker->address,
        'nr_lat_parada' => $faker->latitude,
        'nr_lon_parada' => $faker->longitude
    ];
});