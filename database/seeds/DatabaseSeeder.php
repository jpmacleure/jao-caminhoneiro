<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuariosTableSeeder::class);
        $this->call(BeneficiosTableSeeder::class);
        $this->call(EmblemasTableSeeder::class);
        $this->call(AvaliacaoServicosTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(HabitosTableSeeder::class);
        $this->call(JornadasTableSeeder::class);
        $this->call(PontoParadasTableSeeder::class);
        $this->call(ServicoParadasTableSeeder::class);
        $this->call(TrajetosTableSeeder::class);
        $this->call(UsuarioCheckHabitosTableSeeder::class);
        $this->call(UsuarioCheckinServicosTableSeeder::class);
        $this->call(UsuarioEmblemasTableSeeder::class);
        $this->call(UsuarioHabitosTableSeeder::class);
        $this->call(UsuarioUsoBeneficiosTableSeeder::class);
    }
}
