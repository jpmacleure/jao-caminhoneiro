<?php

use Illuminate\Database\Seeder;

class EmblemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Emblema::class, 65)->create()->each(function ($emblema) {
            //$ativo->save();
        });
    }
}
