<?php

use Illuminate\Database\Seeder;

class BeneficiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Beneficio::class, 10)->create()->each(function ($beneficio) {
            //$ativo->save();
        });
    }
}
