<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//implementadas
Route::view('/cadastro', 'cadastro');
Route::view('/login', 'login');
Route::view('/home', 'home');
Route::view('/jornada', 'jornada');
Route::view('/iniciar-jornada', 'iniciar-jornada');
Route::view('/historico', 'historico');
Route::view('/detalhe-habito', 'detalhe-habito');
Route::view('/emblema', 'emblema');

//falta
Route::view('/beneficio', 'beneficio');
Route::view('/contato', 'contato');