<!-- 

### Added - para features adicionadas
### Removed - para features removidas
### Fixed - para correções no código
### Changed - para features modificadas

-->

# Release Notes

## [Unreleased]

## [0.5.0] - 14/06/2020

### Added
- Popula factories
- Popula seeders

## [0.4.0] - 14/06/2020

### Added
- Adiciona telas base
- Adiciona atributos migrations

## [0.3.0] - 14/06/2020

### Added
- Adiciona seeders test database

## [0.2.0] - 14/06/2020

### Added
- Adiciona migrations/factories/controllers/resources routes
- Tabelas: usuario, trajeto, jornada, ponto_parada, servico_parada, beneficio
    avaliacao_servico, usuario_checkin_servico, usuario_uso_beneficio, habito
    usuario_habito, usuario_check_habito, emblema, usuario_emblema, contato

### Removed
- Remove função não utilizada no sw.js

## [0.1.0] - 13/06/2020

### Added
- Adiciona estrutura PWA
- Adiciona README
- Adiciona Dockerfile e docker-compose.yml

